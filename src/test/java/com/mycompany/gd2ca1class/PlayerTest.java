/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2ca1class;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author administrator
 */
public class PlayerTest {
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Player.
     */
    @Ignore
    public void testGetName() {
        System.out.println("getName");
        Player instance = null;
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHitPoints method, of class Player.
     */
    @Ignore
    public void testGetHitPoints() {
        System.out.println("getHitPoints");
        Player instance = null;
        int expResult = 0;
        int result = instance.getHitPoints();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLives method, of class Player.
     */
    @Ignore
    public void testGetLives() {
        System.out.println("getLives");
        Player instance = null;
        int expResult = 0;
        int result = instance.getLives();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAttack method, of class Player.
     */
    @Ignore
    public void testGetAttack() {
        System.out.println("getAttack");
        Player instance = null;
        int expResult = 0;
        int result = instance.getAttack();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDefence method, of class Player.
     */
    @Ignore
    public void testGetDefence() {
        System.out.println("getDefence");
        Player instance = null;
        int expResult = 0;
        int result = instance.getDefence();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentWeapon method, of class Player.
     */
    @Ignore
    public void testGetCurrentWeapon() {
        System.out.println("getCurrentWeapon");
        Player instance = null;
        Weapon expResult = null;
        Weapon result = instance.getCurrentWeapon();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInventory method, of class Player.
     */
    @Ignore
    public void testGetInventory() {
        System.out.println("getInventory");
        Player instance = null;
        ArrayList<Weapon> expResult = null;
        ArrayList<Weapon> result = instance.getInventory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of draw method, of class Player.
     */
    @Ignore
    public void testDraw() {
        System.out.println("draw");
        Player instance = null;
        instance.draw();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of attackMonster method, of class Player.
     */
    @Test
    public void testAttackMonster() {
        System.out.println("attackMonster");
        Monster monster = new Monster("Orc", 10, 10, 5, new Weapon("Axe"), new Position(0,0,0), new Vector3(0, 10, 0));
        ArrayList<Weapon> inventory = new ArrayList<>();
        inventory.add(new Weapon("Sword"));
        Player hero = new Player("Hero", 100, 5, 100, 10, new Weapon("Sword"), inventory, new Position(0,0,0), new Vector3(0,0,0));
        hero.attackMonster(monster);
        int expectedHitPoints = 0;
        int actualHitPoints = monster.getHitPoints();
        assertEquals(expectedHitPoints, actualHitPoints);
    }

    /**
     * Test of takeDamage method, of class Player.
     */
    @Test
    public void testTakeDamage() {
        System.out.println("takeDamage");
        int damage = 100;
        ArrayList<Weapon> inventory = new ArrayList<>();
        inventory.add(new Weapon("Sword"));
        Player hero = new Player("Hero", 100, 5, 100, 10, new Weapon("Sword"), inventory, new Position(0,0,0), new Vector3(0,0,0));
        hero.takeDamage(damage);
        int expectedHitPoints = 0;
        int actualHitPoints = hero.getHitPoints();
        assertEquals(expectedHitPoints, actualHitPoints);
    }

    /**
     * Test of isAlive method, of class Player.
     */
    @Ignore
    public void testIsAlive() {
        System.out.println("isAlive");
        Player instance = null;
        boolean expResult = false;
        boolean result = instance.isAlive();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of changeWeapon method, of class Player.
     */
    @Test
    public void testChangeWeapon() {
        System.out.println("changeWeapon");
        String weaponName = "Pineapple";
        ArrayList<Weapon> inventory = new ArrayList<>();
        inventory.add(new Weapon("Sword"));
        Player hero = new Player("Hero", 100, 5, 100, 10, new Weapon("Sword"), inventory, new Position(0,0,0), new Vector3(0,0,0));
        boolean expResult = false;
        boolean result = hero.changeWeapon(weaponName);
        assertEquals(expResult, result);
    }
    
}
