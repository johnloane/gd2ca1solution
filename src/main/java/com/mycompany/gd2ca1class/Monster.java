/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2ca1class;

/**
 *
 * @author administrator
 */
public class Monster extends GameEntity{
    public static int monsterCount;
    private String type;
    private int hitPoints;
    private int attack;
    private int defence;
    private Weapon weapon;

    public Monster(String type, int hitPoints, int attack, int defence, Weapon weapon, Position position, Vector3 velocity) {
        super(position, velocity);
        this.type = type;
        this.hitPoints = hitPoints;
        this.attack = attack;
        this.defence = defence;
        this.weapon = weapon;
        monsterCount++;
    }

    public static int getMonsterCount() {
        return monsterCount;
    }

    public String getType() {
        return type;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public Weapon getWeapon() {
        return weapon;
    }
    
    @Override
    public void draw(){
        System.out.println("Drawing a monster");
    }
    
    /**
     * Monsters attack players repeatedly until the monster
     * or the player dies
     * Damage is calculated as monster attack - player defense
     * @param player 
     */
    public void attackPlayer(Player player){
        int result = attack - player.getDefence();
        
        if(result > 0)
        {
            System.out.println("The player takes " + result + " damage");
            player.takeDamage(result);
        }
        else
        {
            System.out.println("The monster attacks and misses");
        }
    }
    
    public void takeDamage(int damage){
        this.hitPoints -= damage;
        if(this.hitPoints < 0){
            this.hitPoints = 0;
        }
    }
    
    public boolean isAlive(){
        if(this.hitPoints <= 0){
            monsterCount--;
            System.out.println("The monster has died. Game over");
            return false;
        }
        return true;
    }
    
}
