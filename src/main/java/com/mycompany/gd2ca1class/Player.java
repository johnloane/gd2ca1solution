/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2ca1class;

import java.util.ArrayList;

/**
 *
 * @author administrator
 */
public class Player extends GameEntity{
    private String name;
    private int hitPoints;
    private int lives;
    private int attack;
    private int defence;
    private Weapon currentWeapon;
    private ArrayList<Weapon> inventory;

    public Player(String name, int hitPoints, int lives, int attack, int defence, Weapon currentWeapon, ArrayList<Weapon> inventory, Position position, Vector3 velocity) {
        super(position, velocity);
        this.name = name;
        this.hitPoints = hitPoints;
        this.lives = lives;
        this.attack = attack;
        this.defence = defence;
        this.currentWeapon = currentWeapon;
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getLives() {
        return lives;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public ArrayList<Weapon> getInventory() {
        return inventory;
    }
    
    @Override
    public void draw(){
        System.out.println("Drawing the player");
    }
    
    /**
     * attackMonster allows the player to attack a monster
     * uses player attack - monster defence to work out the damage
     * @param monster 
     */
    
    public void attackMonster(Monster monster){
        int result = attack - monster.getDefence();
        
        if(result > 0)
        {
            System.out.println("The monster takes " + result + " damage");
            monster.takeDamage(result);
        }
        else
        {
            System.out.println("The player attacks and misses");
        }
    }
    
    public void takeDamage(int damage){
        this.hitPoints -= damage;
        if(this.hitPoints < 0){
            this.hitPoints = 0;
        }
    }
    
    public boolean isAlive(){
        if(this.hitPoints <= 0){
            System.out.println("The player has died. Game over");
            return false;
        }
        return true;
    }
    /**
     * changeWeapon allow the player to change their weapon
     * if they have the selected weapon
     * @param weaponName
     * @return 
     */
    public boolean changeWeapon(String weaponName){
        boolean weaponFound = false;
        for(Weapon weapon : inventory){
            if(weapon.getName().equals(weaponName)){
                weaponFound = true;
                this.currentWeapon = weapon;
                return true;
            }
        }
        if(weaponFound == false)
        {
            System.out.println("You don't have that weapon");
        }
        return false;
    }
    
    
}
