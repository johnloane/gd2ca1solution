/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2ca1class;

/**
 *
 * @author administrator
 */
public abstract class GameEntity {
    private Position position;
    private Vector3 velocity;

    public GameEntity(Position position, Vector3 velocity) {
        this.position = position;
        this.velocity = velocity;
    }
    
    public abstract void draw();

    public Position getPosition() {
        return position;
    }

    public Vector3 getVelocity() {
        return velocity;
    }

    @Override
    public String toString() {
        return "GameEntity{" + "position=" + position + ", velocity=" + velocity + '}';
    }
}
